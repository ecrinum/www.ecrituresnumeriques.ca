/**
 * Dictionnaire des slugs pour les différentes sections du site, avec leur correspondance fr/en.
 * 
 * @type {Map<String, Array<{lang: String, slug: String}>>}
 */
export default new Map()
  .set('home', [
    { lang: 'fr', slug: '' },
    { lang: 'en', slug: '' },
  ])
  .set('teamIndex', [
    { lang: 'fr', slug: 'equipe' },
    { lang: 'en', slug: 'team' }
  ])
  .set('events', [
    { lang: 'fr', slug: 'evenements' },
    { lang: 'en', slug: 'events' }
  ])
  .set('persons', [
    { lang: 'fr', slug: 'personnes' },
    { lang: 'en', slug: 'personnes' }
  ])
  .set('projects', [
    { lang: 'fr', slug: 'projets' },
    { lang: 'en', slug: 'projects' }
  ])
  .set('publications', [
    { lang: 'fr', slug: '' },
    { lang: 'en', slug: '' }
  ])
  .set('axis', [
    { lang: 'fr', slug: 'axes-de-recherche' },
    { lang: 'en', slug: 'research-axis' }
  ])
  .set('keyConcepts', [
    { lang: 'fr', slug: 'concepts-cles' },
    { lang: 'en', slug: 'key-concepts' }
  ])
  .set('researchObjects', [
    { lang: 'fr', slug: 'objets-de-recherche' },
    { lang: 'en', slug: 'research-objects' }
  ])
  .set('researchFields', [
    { lang: 'fr', slug: 'champs-de-recherche' },
    { lang: 'en', slug: 'research-fields' }
  ])

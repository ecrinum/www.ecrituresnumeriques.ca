import { directusClient } from '$lib/directus';
import { aggregate, readItems } from '@directus/sdk';
import { error } from '@sveltejs/kit';

/**
 * Données pour l'index des événements
 * 
 * @type {import('./$types').PageServerLoad}
 */
export async function load({ params, url }) {
  const { lang } = params;

  /**
   * Le searchParam `?page=` dans l'URL, pour la pagination des résultats
   * (attention, le SDK Directus requiert un type Number et non String)
   * @type {Number}
   */
  const page = Number(url.searchParams.get('page') || 1);
  
  /**
   * Nombre d'événements à afficher par page
   * @type {Number}
   */
  const perPage = 11;

  try {
    /**
     * Les plus récents événements, avec pagination
     * 
     * @type {Record<String, any>[]}
     * @throws Une exception sera lancée s'il y a un problème dans la requête
     */
    const eventsData = await directusClient.request(
      readItems('events', {
        fields: [
          '*',
          'translations.*',
        ],

        // tri par date décroissante
        sort: '-date_start',

        // limiter le nombre d'événements par page
        limit: perPage,

        // si la pagination est spécifiée (1 par défaut)
        page,

        // on récupère uniquement la traduction dans la `lang` actuelle
        deep: {
          translations: {
            _filter: {
              languages_code: {
                _eq: lang
              }
            }
          },
        }
      })
    );

    /**
     * Données d'agrégation, pour la pagination
     * 
     * La définition TypeScript semble ne pas fonctionner correctement;
     * la charge utile ressemble à ceci (`count` retourne un string):
     * {Array<{count: string}>}
     * 
     * @throws Une exception sera lancée s'il y a un problème dans la requête
     */
    const eventsAggregateData = await directusClient.request(
      aggregate('events', {
        aggregate: {
          count: '*'
        }
      })
    );

    /**
     * On utilise la l'agrégation pour déterminer le nombre total
     * d'items pour pouvoir afficher la pagination (`next`) dans les
     * données de la page.
     * @type {Number}
     */
    const totalItems = Number(eventsAggregateData[0]?.count || -1);

    /**
     * @type {{ page: number; perPage: number; next: number | null; items: any[]; }}
     */
    return {
      /**
       * Page actuelle (la première page est la page 1)
       * @type {Number}
       */
      page,
      /**
       * Nombre maximum d'items par page (constante)
       * @type {Number}
       */
      perPage,
      /**
       * La propriété `next` (pour nextPage) permet de parcourir
       * les données de manière paginée.
       * La valeur est `null` s'il n'y a pas de page suivante.
       * @type {Number | null}
       */
      next: page * perPage < totalItems ? page + 1 : null,

      /**
       * Nombre total d'items (constante)
       * @type {Number}
       */
      totalItems,

      items: eventsData.map(event => {
        const {
          slug,
          date_start,
          date_end,
          time_start,
          time_end,
          event_type,
          translations,
        } = event;

        return {
          slug,
          date_start,
          date_end,
          time_start,
          time_end,
          event_type,
          // aplatissement de la traduction avec l'opérateur spread `...`
          ...translations[0]
        };
      })
    };
  } catch (e) {
    console.error(e);

    // autre erreur, que nous considérons comme une erreur interne
    error(500, {
      message: 'Cette page n’est pas disponible en ce moment (erreur interne). Veuillez revenir plus tard.'
    });
  }
}
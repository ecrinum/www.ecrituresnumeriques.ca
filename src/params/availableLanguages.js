import { PUBLIC_AVAILABLE_LANGUAGES } from '$env/static/public';

/**
 * Valider le paramètre de langue.
 * 
 * @type {import('@sveltejs/kit').ParamMatcher}
 */
export function match(param) {
  /**
   * Les langues disponibles doivent être listées au format ISO-639 (code court)
   * et séparées par des virgules (CSV)
   * @type {Array<String>}
   */
  const siteLanguages = PUBLIC_AVAILABLE_LANGUAGES.split(',');

  /** @type {Boolean} */
  return siteLanguages.includes(param);
}

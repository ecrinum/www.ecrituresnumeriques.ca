// fichier pour permettre l'importation facile de toutes les locales
import fr from './fr';
import en from './en';

/**
 * @prop {object} fr
 * @prop {object} en
 */
export default {
  fr,
  en,
};

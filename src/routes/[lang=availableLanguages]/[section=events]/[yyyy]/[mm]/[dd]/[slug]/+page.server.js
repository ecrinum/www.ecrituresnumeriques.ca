import { directusClient } from '$lib/directus';
import { readItems } from '@directus/sdk';
import { error } from '@sveltejs/kit';

/**
 * Données pour une page événement
 * 
 * @type {import('./$types').PageServerLoad}
 */
export async function load({ params }) {
  const { lang, slug } = params;

  try {
    /**
     * Requête pour obtenir l'événement avec le `slug` actuel
     * 
     * @type {Record<String, any>[]}
     * @throws Une exception sera lancée s'il y a un problème dans la requête
     */
    const queryItemsData = await directusClient.request(
      readItems('events', {
        fields: [
          '*',
          'translations.*'
        ],

        // on récupère l'item selon le `slug` et la `lang`
        filter: {
          translations: {
            _and: [
              {
                slug: {
                  _eq: slug
                }
              },
              {
                languages_code: {
                  _eq: lang
                }
              },
            ]
          }
        },

        // on limite à 1 résultat (le seul qui devrait être bon de toute façon)
        limit: 1,

        // on ne retient dans la charge utile que la traduction dans
        // la `lang` actuelle, ainsi qu'avec le `slug` correspondant
        deep: {
          translations: {
            _filter: {
              _and: [
                {
                  languages_code: {
                    _eq: lang
                  },
                },
                {
                  slug: {
                    _eq: slug
                  }
                }
              ]
            }
          },
        }
      })
    );

    if (!queryItemsData.length) {
      // aucun résultat, c'est un cas de 404
      // on arrête l'exécution ici; la suite dans le bloc `catch`
      // la méthode SvelteKit lance une erreur de type `HttpError`
      error(404);
    }

    const {
      date_start,
      date_end,
      time_start,
      time_end,
      event_type,
      event_languages,
      translations,
    } = queryItemsData[0];

    /**
     * @type {{ date_start: string; }}
     */
    return {
      date_start,
      date_end,
      time_start,
      time_end,
      event_type,
      event_languages,
      // aplatissement de la traduction avec l'opérateur spread `...`
      ...translations[0]
    };
  } catch (/** @type {import('@sveltejs/kit').HttpError | any} */e) {
    // erreur de type HttpError, lancée plus haut avec SvelteKit
    if (e?.status === 404) {
      error(404, {
        message: lang === 'en' ? 'The page could not be found.' : 'La page demandée est introuvable.'
      });
    }

    // autre erreur, que nous considérons comme une erreur interne
    error(500, {
      message: 'Cette page n’est pas disponible en ce moment (erreur interne). Veuillez revenir plus tard.'
    });
  }
}

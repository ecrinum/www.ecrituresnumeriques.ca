import { createDirectus, rest } from '@directus/sdk';
import { PUBLIC_DIRECTUS_URL } from '$env/static/public';

if (!PUBLIC_DIRECTUS_URL?.length) {
  throw new Error('La variable d’environnement "PUBLIC_DIRECTUS_URL" n’est pas remplie. Avez-vous renseigné cette variable dans le fichier ".env"?');
}

/**
 * Directus Client
 * Pour réutilisation facile à travers l'application, avec les
 * fonctionnalités d'API (request, readItem, etc.)
 * @see https://docs.directus.io/guides/sdk/getting-started.html
 * @public
 */
export const directusClient = createDirectus(PUBLIC_DIRECTUS_URL).with(rest());

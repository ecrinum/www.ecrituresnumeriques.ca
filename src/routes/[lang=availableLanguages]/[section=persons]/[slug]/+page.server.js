import { directusClient } from '$lib/directus';
import { readItems } from '@directus/sdk';
import { error } from '@sveltejs/kit';

/**
 * Données pour la page d'une personne
 * 
 * @type {import('./$types').PageServerLoad}
 */
export async function load({ params }) {
  const { lang, slug } = params;

  try {
    /**
     * La personne avec le `slug` actuel
     * 
     * @type {Record<String, any>[]}
     * @throws Une exception sera lancée s'il y a un problème dans la requête
     */
    const queryPersonsData = await directusClient.request(
      readItems('persons', {
        fields: [
          '*',
          'translations.*',
        ],

        // on récupère l'item selon le `slug`
        // (le `slug` n'est pas traduit pour les personnes)
        filter: {
          slug: {
            _eq: slug
          }
        },

        // on limite à 1 résultat (le seul qui devrait être bon de toute façon)
        limit: 1,

        // on ne retient que la traduction dans la `lang` actuelle
        deep: {
          translations: {
            _filter: {
              languages_code: {
                _eq: lang
              },
            }
          },
        }
      })
    );

    if (!queryPersonsData.length) {
      // aucun résultat, c'est un cas de 404
      // on arrête l'exécution ici; la suite dans le bloc `catch`
      // la méthode SvelteKit lance une erreur de type `HttpError`
      error(404);
    }

    const {
      /** @type {String} */
      first_name,
      /** @type {String} */
      last_name,
      /** @type {String} */
      email,
      /** @type {Boolean} */
      is_alumnus,
      /** @type {String} */
      image_id,
      /** @type {String} */
      legacy_image,
      /** @type {Array<any>} */
      translations,
    } = queryPersonsData[0];

    /**
     * @type {{
     * title: string;
     * first_name: string;
     * last_name: string;
     * email: string;
     * is_alumnus: boolean;
     * image_id: string;
     * }}
     */
    return {
      title: `${first_name} ${last_name}`,
      description: `Profil de ${first_name.trim()} ${last_name.trim()}.`,
      first_name,
      last_name,
      email,
      is_alumnus,
      image_id,
      legacy_image,
      // aplatissement de la traduction avec l'opérateur spread `...`
      ...translations[0]
    };
  } catch (/** @type {import('@sveltejs/kit').HttpError | any} */e) {
    // Cela pourrait être une erreur `HttpError`, lancée plus haut
    // avec SvelteKit
    if (e?.status === 404) {
      error(404, {
        message: lang === 'en' ? 'The page could not be found.' : 'La page demandée est introuvable.'
      });
    }

    // autre erreur, que nous considérons comme une erreur interne
    console.error(e);
    error(500, {
      message: 'Cette page n’est pas disponible en ce moment (erreur interne). Veuillez revenir plus tard.'
    });
  }
}

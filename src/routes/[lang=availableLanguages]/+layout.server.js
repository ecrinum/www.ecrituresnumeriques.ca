import { directusClient } from '$lib/directus';
import { readSingleton } from '@directus/sdk';
import { error } from '@sveltejs/kit';

/**
 * Données globales pour toutes les pages
 * 
 * @type {import('./$types').LayoutServerLoad}
 */
export async function load({ params }) {
  const { lang } = params;

  try {
    /**
     * L'objet pour les réglages du site 
     * 
     * @type {Record<String, {translations: any[]}>}}
     * @throws Une exception sera lancée s'il y a un problème dans la requête
     */
    const settingsData = await directusClient.request(
      readSingleton('settings', {
        fields: [
          '*',
          'translations.*',
          'translations.site_footer_logos.directus_files_id',
          'translations.site_footer_logos.filename_download',
          'translations.site_footer_logos.title',
        ],

        // on récupère uniquement la traduction dans la langue actuelle
        // ce qui correspond au champ `languages_code` dans Directus
        deep: {
          translations: {
            _filter: {
              languages_code: {
                _eq: lang
              }
            }
          }
        }
      })
    );

    /**
     * Groupe de champs Analytics
     */
    const {
      plausible_domain,
      plausible_api_host
    } = settingsData;

    // l'objet retourné inclut les données de la traduction
    // (une seule traduction, celle correspondant à la `lang`, devrait figurer
    // dans l'array `translations`, d'où l'indice [0])
    /**
     * @type {{ lang: string; }}
     */
    return {
      lang,

      // analytics
      plausible_domain,
      plausible_api_host,

      // aplatissement avec l'opérateur spread `...`
      ...settingsData.translations[0],
    };
  } catch (/** @type {{status?: Number, text?: String}} */e) {
    console.error({ e });

    // ici, c'est vraiment un cas d'erreur interne (code 500)
    error(500, {
      message: 'Erreur interne: les réglages du site ne sont pas accessibles.'
    });
  }
}
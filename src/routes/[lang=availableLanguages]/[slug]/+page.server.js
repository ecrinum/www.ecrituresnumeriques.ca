import { directusClient } from '$lib/directus';
import { readItems } from '@directus/sdk';
import { error } from '@sveltejs/kit';

/**
 * Données pour une page ordinaire
 * 
 * @type {import('./$types').PageServerLoad}
 */
export async function load({ params }) {
  const { lang, slug } = params;

  try {
    /**
     * Requête pour obtenir l'item avec le `slug` actuel
     * 
     * @type {Record<String, any>[]}
     * @throws Une exception sera lancée s'il y a un problème dans la requête
     */
    const queryItemsData = await directusClient.request(
      readItems('pages', {
        fields: [
          '*',
          'translations.*',
          // Les blocs (via le champ blocks_relations) sont Many-to-Any (M2A)
          // et demandent un peu plus de travail d'explicitation.
          // Il faut par ailleurs imbriquer chaque bloc sous `item`.
          {
            translations: [{
              blocks_relations: [
                // Il est important d'avoir le nom de la collection
                // ex. est-ce un bloc de type alerte, richtext, etc.
                'collection',
                {
                  item: {
                    // 1. bloc texte enrichi
                    richtext_block: ['content_html'],
                    // 2. bloc figure
                    figure_block: ['image_id', 'caption_html'],
                    // 3. bloc alerte
                    alert_block: ['content_html', 'variant'],
                  },
                }
              ]
            }]
          },
        ],

        // on récupère l'item selon le `slug` et la `lang`
        filter: {
          translations: {
            _and: [
              {
                slug: {
                  _eq: slug
                }
              },
              {
                languages_code: {
                  _eq: lang
                }
              },
            ]
          }
        },

        // on limite à 1 résultat (le seul qui devrait être bon de toute façon)
        limit: 1,

        // on ne retient dans la charge utile que la traduction dans
        // la `lang` actuelle, ainsi qu'avec le `slug` correspondant
        deep: {
          translations: {
            _filter: {
              languages_code: {
                _eq: lang
              },
            },
          },
        }
      })
    );

    if (!queryItemsData.length) {
      // aucun résultat, c'est un cas de 404
      // on arrête l'exécution ici; la suite dans le bloc `catch`
      // la méthode SvelteKit lance une erreur de type `HttpError`
      error(404);
    }

    // on récupère le premier résultat de la requête
    // (il ne devrait y en avoir qu'un seul, avec le `slug` correspondant)
    const {
      id,
      translations,
    } = queryItemsData[0];
    
    /**
     * @type {{ relatedProjects: Array<any>; relatedEvents: Array<any>; }}
     */
    return {
      // aplatissement de la traduction avec l'opérateur spread `...`
      ...translations[0]
    };
  } catch (/** @type {import('@sveltejs/kit').HttpError | any} */e) {
    // Cela pourrait être une erreur `HttpError`, lancée plus haut
    // avec SvelteKit
    if (e?.status === 404) {
      error(404, {
        message: lang === 'en' ? 'The page could not be found.' : 'La page demandée est introuvable.'
      });
    }

    // autre erreur, que nous considérons comme une erreur interne
    console.error(e);
    error(500, {
      message: 'Cette page n’est pas disponible en ce moment (erreur interne). Veuillez revenir plus tard.'
    });
  }
}

import { directusClient } from '$lib/directus';
import { readItems } from '@directus/sdk';
import { error } from '@sveltejs/kit';
import localeStrings from '$lib/locales/index';

/**
 * Données pour la page d'index des objets de recherche
 * 
 * @type {import('./$types').PageServerLoad}
 */
export async function load({ params }) {
  const { lang } = params;

  try {
    /**
     * Requêter tous les objets de recherche
     * 
     * @type {Record<String, any>[]}
     * @throws Une exception sera lancée s'il y a un problème dans la requête
     */
    const itemsData = await directusClient.request(
      readItems('research_objects', {
        fields: [
          '*',
          'translations.*',
        ],

        // on veut tous les items
        // la valeur `-1` permet d'éviter la limite (pas de pagination)
        limit: -1,

        // on récupère uniquement la traduction dans la `lang` actuelle
        deep: {
          translations: {
            _filter: {
              languages_code: {
                _eq: lang
              }
            }
          },
        }
      })
    );

    /**
     * @type {{ items: any[] }}
     */
    return {
      // le titre est obtenu via l'un des fichiers de traduction
      title: localeStrings.hasOwnProperty(lang) ?
        localeStrings[lang]?.research_objects :
        null,

      items: itemsData.map(item => {
        const {
          /** @type {String} */
          title,
          /** @type {Array<any>} */
          translations,
        } = item;

        return {
          title,
          // aplatissement de la traduction avec l'opérateur spread `...`
          ...translations[0]
        };
      })
    };
  } catch (e) {
    console.error(e);

    // il pourrait s'agir d'une erreur interne (code 500), ou d'un problème de
    // connectivité, indisponibilité de la plateforme de données, etc.;
    // mais pour le public, nous afficherons une erreur 404
    error(404, {
      message: 'La page est introuvable, ou n’est pas disponible pour le moment.'
    });
  }
}
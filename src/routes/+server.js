import { redirect } from '@sveltejs/kit';
import { PUBLIC_AVAILABLE_LANGUAGES, PUBLIC_DEFAULT_LANGUAGE } from '$env/static/public';

/**
 * Effectuer la redirection vers la langue par défaut
 * 
 * @type {import('./$types').RequestHandler}
 */
export function GET() {
  // Si la langue par défaut est définie, on redirige vers elle;
  // sinon, on redirige vers la première langue parmi l'ensemble
  // des langues définies pour le site
  redirect(
    307,
    PUBLIC_DEFAULT_LANGUAGE?.length ?
      `/${PUBLIC_DEFAULT_LANGUAGE}` :
      `/${PUBLIC_AVAILABLE_LANGUAGES.split(',')[0]}`
  );
}

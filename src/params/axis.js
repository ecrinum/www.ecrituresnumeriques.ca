import slugsMap from '$lib/slugsMap';

/**
 * Valider le chemin de la page d'équipe.
 * 
 * @type {import('@sveltejs/kit').ParamMatcher}
 */
export function match(param) {
  // On retourne une collection de slugs de type Array<String>,
  // puis on valide simplement si le slug s'y trouve -- peu importe la langue.
  // L'opérateur `?` existe si jamais slugsMap n'a pas d'entrée définie pour
  // 'axis' (on s'assure de retourner un booléen)
  return slugsMap.get('axis')
    ?.map(entry => entry.slug)
    .includes(param)
    || false;
}

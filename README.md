# Site web | Chaire de recherche du Canada sur les écritures numériques

Ce dépôt contient les sources pour générer le site web de la Chaire de recherche du Canada sur les écritures numériques.
Il s'agit d'un projet [SvelteKit](https://kit.svelte.dev/).

La source des données est une [installation Directus](https://gitlab.huma-num.fr/ecrinum/plateforme-donnees-crcen), qui permet de modéliser des ensembles de données relationnelles grâce à une interface facile d'utilisation.

## Objectifs

- **Favoriser l'utilisation horizontale par le plus de membres de l'équipe possible.** Il doit être possible pour chacun·e d'intervenir sur le site, d'une manière ou d'une autre.
- **Mettre en valeur les données.** Les sites web vivent et meurent, mais le recours à des formats d'échange standards et ouverts facilitent la réutilisation, et ulitimement la conservation à long terme des travaux.
- **Utiliser des technologies libres**, respectueuses des droits et valeurs des utilisateurs.

## Installation

Une instance Directus (plateforme de données alimentant le site) doit être installée séparément.

Remplir les variables d'environnement à partir du fichier `.env.example`.
Dupliquer ce fichier en le nommant `.env` :

```bash
# dupliquer .env.example => .env
cp .env.example .env
```

Ouvrir le fichier `.env` pour édition et remplir les valeurs obligatoires, en renseignant par exemple l'adresse de l'installation Directus.

Les langues du site doivent être renseignées dans la variable `PUBLIC_AVAILABLE_LANGUAGES`.

---

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.

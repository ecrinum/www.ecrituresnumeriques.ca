import { addMessages, init } from 'svelte-i18n';
import fr from './locales/fr';
import en from './locales/en';

addMessages('en', en);
addMessages('fr', fr);

/**
 * @param {string} lang
 * @usage bootstrap('fr')
 */
export function bootstrap(lang) {
  init({
    fallbackLocale: 'fr',
    initialLocale: lang,
  });
}

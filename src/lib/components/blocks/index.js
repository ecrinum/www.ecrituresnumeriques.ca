/**
 * Dictionnaire des blocs, destiné à être employé par `BlocksZone.svelte`.
 * Un objet `components` est exporté, permettant de raccorder le nom
 * d'un bloc dans le CMS (ex. "richtext_block") et son implémentation dans
 * le front (ex. "RichtextBlock.svelte").
 */
import RichtextBlock from './RichtextBlock.svelte';
import AlertBlock from './AlertBlock.svelte';
import FigureBlock from './FigureBlock.svelte';

export const components = {
  richtext_block: RichtextBlock,
  alert_block: AlertBlock,
  figure_block: FigureBlock,
};

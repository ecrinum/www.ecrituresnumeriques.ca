/**
 * Les clés sont disposées en ordre alphabétiques.
 * Certaines traductions sont imbriquées dans un espace de nom;
 * elles sont situées plutôt à la fin.
 */
export default {
  'activity': 'Activity',
  'activities': 'Activities',
  'address': 'Address',
  'alumnus': 'Alumnus',
  'alumni': 'Alumni',
  'axis': 'Axe de recherche',
  'bibliographc_references': 'Bibliographic references',
  'axis_plural': 'Axes de recherche',
  'concepts': 'Concepts',
  'context': 'Context',
  'data': 'Data',
  'date': 'Date',
  'date_end': 'End date',
  'date_start': 'Start date',
  'description': 'Description',
  'details': 'Details',
  'done': 'Done',
  'email': 'Email',
  'error': 'Error',
  'event': 'Event',
  'events': 'Events',
  'home': 'Home',
  'last_modified_on': 'Last modified on',
  'licence': 'License',
  'link': 'Link',
  'links': 'Links',
  'key_concept': 'Key concept',
  'key_concepts': 'Key concepts',
  'next_events': 'Next events',
  'ongoing': 'Ongoing',
  'page_data': 'Page data',
  'person': 'Person',
  'persons': 'Persons',
  'phone': 'Phone',
  'project': 'Project',
  'projects': 'Projects',
  'reference_this_page': 'To reference this page',
  'reference': 'Reference',
  'related_event': 'Related event',
  'related_events': 'Related events',
  'related_project': 'Related project',
  'related_projects': 'Related projects',
  'research_field': 'Research field',
  'research_fields': 'Research fields',
  'research_object': 'Research object',
  'research_objects': 'Research objects',
  'see_more': 'See more',
  'see_more_ellipsis': 'See more…',
  'show_hide': 'Show/hide',
  'team': 'Team',

  /* Espaces de nom */
  
  // page d'accueil
  'homepage': {
    'our_research_fields': 'Our research fidlds',
    'see_all_events': 'See all events',
  },
  // page d'équipe
  'teamIndex': {
    'active_members': 'Active members',
    'occupation_status': 'Occupation/status',
    'role': 'Rple',
  },

  // page événement
  'event_page': {
    'event_languages': 'Event languages',
    'event_type': 'Event type',
    'event_type_list': {
      'book_launch': 'Launch',
      'colloquium': 'Colloquium',
      'conference': 'Conference',
      'interview': 'Interview',
      'talk': 'Talk',
      'workshop': 'Workshop',
    },
    'time_end': 'Time of end',
    'time_start': 'Time of start',
  },

  // langues
  'languages_list': {
    'de': 'Allemand',
    'en': 'Anglais',
    'fr': 'Français',
    'es': 'Espagnol',
    'el': 'Grec',
    'it': 'Italien',
    'la': 'Latin',
  },
}

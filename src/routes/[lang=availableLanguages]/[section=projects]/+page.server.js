import { directusClient } from '$lib/directus';
import { readItems } from '@directus/sdk';
import { error } from '@sveltejs/kit';
import localeStrings from '$lib/locales/index';

/**
 * Données pour la page projets
 * 
 * @type {import('./$types').PageServerLoad}
 */
export async function load({ params }) {
  const { lang } = params;

  try {
    /**
     * L'ensemble des projets publiés
     * 
     * @type {Record<String, any>[]}
     * @throws Une exception sera lancée s'il y a un problème dans la requête
     */
    const projectsData = await directusClient.request(
      readItems('projects', {
        fields: [
          '*',
          'translations.*',
        ],

        // on récupère uniquement la traduction dans la `lang` actuelle
        deep: {
          translations: {
            _filter: {
              languages_code: {
                _eq: lang
              }
            }
          },
        }
      })
    );

    /**
     * @type {{ projects: any[] }}
     */
    return {
      // le titre est obtenu via l'un des fichiers de traduction
      title: localeStrings.hasOwnProperty(lang) ?
        localeStrings[lang]?.['projects'] :
        null,

      projects: projectsData.map(project => {
        const {
          slug,
          date_start,
          date_end,
          project_status,
          feature_image,
          translations,
        } = project;

        return {
          slug,
          date_start,
          date_end,
          project_status,
          feature_image,
          // aplatissement de la traduction avec l'opérateur spread `...`
          ...translations[0]
        };
      })
    };
  } catch (e) {
    console.error(e);

    // il pourrait s'agir d'une erreur interne (code 500), ou d'un problème de
    // connectivité, indisponibilité de la plateforme de données, etc.;
    // mais pour le public, nous afficherons une erreur 404
    error(404, {
      message: 'La page projets n’est pas disponible en ce moment (erreur interne). Veuillez réessayer plus tard.'
    });
  }
}
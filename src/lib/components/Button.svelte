<script>
  /**
   * Bouton réutilisable, avec options de taille, variante et rôle sémantique.
   * @event {{ nativeEvent: MouseEvent }} click
   * @restProps {a | button}
   * 
   * Usage:
   * 
   *   <Button
   *     href="/url"
   *     role="primary"
   *     variant="outline"
   *     size="small"
   *   >
   *     Texte du bouton
   *     <span slot="after">... icône ...</span>
   *   </Button>
   * 
   *   <Button
   *     on:click={ev => ev}
   *     variant="fill"
   *     role="secondary"
   *   >
   *     Texte du bouton
   *   </Button>
   */

  import { createEventDispatcher } from 'svelte';

  // props
  /**
   * Color based on the theme, see `availableColors` for a list of possible values.
   * @type {string}
   * @default 'normal'
   */
  export let role = 'normal';

  /**
   * Variant, see `availableVariants` for a list of possible values.
   * @type {string}
   * @default 'fill'
   */
  export let variant = 'fill';

  /**
   * Size of the button, see `availableSizes` for a list of possible values.
   * @type {string}
   * @default 'normal'
   */
  export let size = 'normal';

  /**
   * Turns the button into a link.
   * @type {string | null}
   */
  export let href = null;

  /**
   * Disables the button, styling it appropriately and disabling click events.
   * @type {boolean}
   */
  export let disabled = false;

  // vars
  const availableRoles = [
    'normal',
    'primary',
    'secondary',
    'danger',
  ];
  const availableVariants = [
    'plein',
    'text',
    'outline',
  ];
  const availableSizes = [
    'small',
    'normal',
    'medium',
    'large',
  ];

  const dispatch = createEventDispatcher();

  // assignations sécuritaires
  if (!availableRoles.includes(role)) {
    role = 'normal'; // par défaut
  }
  if (!availableVariants.includes(variant)) {
    variant = 'plein'; // par défaut
  }
  if (!availableSizes.includes(size)) {
    size = 'normal'; // par défaut
  }
</script>

{#if href}
  <a
    class="Button"
    class:Button--variant-fill={variant === 'plein'}
    class:Button--variant-text={variant === 'text'}
    class:Button--variant-outline={variant === 'outline'}
    class:Button--role-normal={role === 'normal'}
    class:Button--role-primary={role === 'primary'}
    class:Button--role-secondary={role === 'secondary'}
    class:Button--role-danger={role === 'danger'}
    class:Button--size-small={size === 'small'}
    class:Button--size-normal={size === 'normal'}
    class:Button--size-medium={size === 'medium'}
    class:Button--size-large={size === 'large'}
    href={disabled ? null : href}
    on:click={e => dispatch('click', { nativeEvent: e })}
    {...$$restProps}
  >
    {#if $$slots.before}
      <span class="Button__before">
        <slot name="before"/>
      </span>
    {/if}

    <slot />

    {#if $$slots.after}
      <span class="Button__after">
        <slot name="after"/>
      </span>
    {/if}
  </a>
{:else}
  <button
    class="Button"
    class:Button--variant-fill={variant === 'plein'}
    class:Button--variant-text={variant === 'text'}
    class:Button--variant-outline={variant === 'outline'}
    class:Button--role-normal={role === 'normal'}
    class:Button--role-primary={role === 'primary'}
    class:Button--role-secondary={role === 'secondary'}
    class:Button--role-danger={role === 'danger'}
    class:Button--size-small={size === 'small'}
    class:Button--size-normal={size === 'normal'}
    class:Button--size-medium={size === 'medium'}
    class:Button--size-large={size === 'large'}
    on:click={e => dispatch('click', { nativeEvent: e })}
    {...$$restProps}
  >

    {#if $$slots.before}
      <span class="Button__before">
        <slot name="bouton-before"></slot>
      </span>
    {/if}

    <slot />

    {#if $$slots.after}
      <span class="Button__after">
        <slot name="bouton-after"></slot>
      </span>
    {/if}
  </button>
{/if}

<style>
  .Button {
    /* Le bouton est plutôt "horizontal" */
    --Button-spacing-x: 1em;
    --Button-spacing-y: 0.5rem;
    /* Largeur minimale pour un bouton (relativement à la size de la police) */
    --Button-min-width: 6em;
    --Button-color: var(--couleur-texte); /* couleur par défaut */
    --Button-background-color: var(--Button-color);
    --Button-text-color: var(--marine-800);
    --Button-font-size: var(--font-size-xsmall);
    --Button-font: var(--font-details);
    --Button-font-weight: var(--font-weight-details);
    --Button-letter-spacing: 0;
    --Button-line-height: 1;
    --Button-border-width: 0;
    --Button-border-radius: 0;
    --Button-background-image: none;

    padding: var(--Button-spacing-y) var(--Button-spacing-x);
    min-width: var(--Button-min-width);
    display: inline-flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: center;
    align-items: center;
    gap: 0.67em; /* relative */

    border: var(--Button-border-width) solid var(--Button-border-color);
    color: var(--Button-text-color);
    border-radius: var(--Button-border-radius);
    background-color: var(--Button-background-color);
    background-image: var(--Button-background-image);
    background-repeat: no-repeat;
    background-size: 100% 100%;
    background-position: center center;

    font-family: var(--Button-font);
    font-weight: var(--Button-font-weight);
    font-size: var(--Button-font-size);
    letter-spacing: var(--Button-letter-spacing);
    line-height: var(--Button-line-height);
    text-decoration: none; /* annuler les styles de lien */
    text-transform: uppercase;
    /* ne pas créer de retour à la ligne */
    white-space: nowrap;
  }

  /* Slots `before` et `after` */
  .Button__before,
  .Button__after {
    --spacing: 0;
    /* Set exactly 1em height to match line-height and avoid buttons grow */
    height: 1em;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  /* Rôles sémantiques (communiquées via couleurs) */
  .Button--role-primary {
    --Button-color: var(--couleur-texte);
    --Button-text-color: var(--marine-800);
  }
  .Button--role-primary:hover {
    --Button-color: var(--azur-aoste-100);
  }

  .Button--role-secondary {
    --Button-color: var(--marine-200);

    transition: color .15s;
  }
  .Button--role-secondary:hover {
    --Button-color: var(--marine-50)
  }

  .Button--role-danger {
    --Button-color: var(--framboise);
    --Button-text-color: var(--marine-800);
  }
  .Button--role-danger:hover {
    --Button-color: var(--framboise-600);
  }
  .Button--role-danger:active {
    --Button-color: var(--framboise-700);
  }

  /* Variants */
  .Button--variant-normal {
    /* par défaut */
  }
  .Button--variant-outline {
    --Button-border-width: 1px;
    --Button-background-color: transparent;
    --Button-text-color: var(--Button-color);
    --Button-border-color: currentColor;

    transition: opacity .15s, color .15s;
  }
  .Button--variant-outline:hover {
    --Button-color: var(--marine-200);
  }
  .Button--variant-outline:active {
    opacity: .6;
    transition-duration: 0.05s;
  }
  .Button--variant-text {
    --Button-background-color: transparent;
    --Button-text-color: var(--Button-color);
    --Button-min-width: none;
    
    --Button-spacing-x: 0;
  }
  .Button--variant-text:active {
    opacity: .6;
    transition-duration: .05s;
  }

  /* sizes */
  .Button--size-small {
    --Button-spacing-x: 0.875rem;
    --Button-spacing-y: 0.4375em;
    --Button-font-size: var(--font-size-mini);
    --Button-letter-spacing: 0.015em;
  }

  .Button--size-normal {
    /* par défaut */
  }

  .Button--size-medium {
    --Button-spacing-x: .75em;
    --Button-spacing-y: .5em;
    --Button-font-size: var(--typographie-size-corps-2);
    --Button-letter-spacing: 0;
  }

  .Button--size-large {
    --Button-spacing-x: .8333em;
    --Button-spacing-y: .35em;
    --Button-font-size: var(--font-size-medium);
  }
</style>

import { redirect } from '@sveltejs/kit';
import slugsMap from '$lib/slugsMap.js';

/**
 * Nous n'affichons pas une page d'index pour `/:lang/persons`;
 * on redirige donc subrepticement vers la page équipe.
 * La méthode `redirect()` lance une exception, ce qui arrête l'exécution
 * sur-le-champ.
 * 
 * @type {import('./$types').RequestHandler}
 * @throws {import('@sveltejs/kit').Redirect}
 */
export function GET({ params }) {
  const { lang } = params;

  /**
   * Dans le dictionnaire des slugs, on tire l'entrée `teamIndex`
   * dans la `lang` actuelle.
   * 
   * @type {string | undefined}
   */
  const teamIndexSlug = slugsMap.get('teamIndex')
    ?.find(i => i.lang === lang)
    ?.slug;

  // si l'entrée n'est pas définie dans le dictionnaire...
  if (!teamIndexSlug || !teamIndexSlug?.length) {
    // ...redirection vers la page d'accueil
    redirect(307, `/${lang}`);
  } else {
    // le slug est ok; redirection vers la page d'équipe
    redirect(307, `/${lang}/${teamIndexSlug}`);
  }
}

import { directusClient } from '$lib/directus';
import { readItems } from '@directus/sdk';
import { error } from '@sveltejs/kit';

/**
 * Données pour une page concept clé
 * 
 * @type {import('./$types').PageServerLoad}
 */
export async function load({ params }) {
  const { lang, slug } = params;

  try {
    /**
     * Requête pour obtenir l'item avec le `slug` actuel
     * 
     * @type {Record<String, any>[]}
     * @throws Une exception sera lancée s'il y a un problème dans la requête
     */
    const queryItemsData = await directusClient.request(
      readItems('concepts', {
        fields: [
          '*',
          'translations.*',
        ],

        // on récupère l'item selon le `slug` et la `lang`
        filter: {
          translations: {
            _and: [
              {
                slug: {
                  _eq: slug
                }
              },
              {
                languages_code: {
                  _eq: lang
                }
              },
            ]
          }
        },

        // on limite à 1 résultat (le seul qui devrait être bon de toute façon)
        limit: 1,

        // on ne retient dans la charge utile que la traduction dans
        // la `lang` actuelle, ainsi qu'avec le `slug` correspondant
        deep: {
          translations: {
            _filter: {
              _and: [
                {
                  languages_code: {
                    _eq: lang
                  },
                },
                {
                  slug: {
                    _eq: slug
                  }
                }
              ]
            }
          },
        }
      })
    );

    if (!queryItemsData.length) {
      // aucun résultat, c'est un cas de 404
      // on arrête l'exécution ici; la suite dans le bloc `catch`
      // la méthode SvelteKit lance une erreur de type `HttpError`
      error(404);
    }

    // on récupère le premier résultat de la requête
    // (il ne devrait y en avoir qu'un seul, avec le `slug` correspondant)
    const {
      id,
      translations,
    } = queryItemsData[0];
    
    /**
     * Requête pour la relation inverse - projets liés
     * 
     * @type {Record<String, any>[]}
     * @throws Une exception sera lancée s'il y a un problème dans la requête
     */
    const filteredProjectsData = await directusClient.request(
      readItems('projects', {
        fields: [
          '*',
          'translations.*',
        ],

        // filtrer les projets, pour ne retenir que ceux liés
        // au concept actuel
        filter: {
          concepts_relations: {
            concepts_id: {
              _eq: id
            }
          }
        },

        // désactiver la limite de pagination (au cas où)
        limit: -1,

        // on ne retient dans la charge utile que la traduction dans
        // la `lang` actuelle, ainsi qu'avec le `slug` correspondant
        deep: {
          concepts_relations: {
            concepts_id: {
              translations: {
                _filter: {
                  _and: [
                    {
                      languages_code: {
                        _eq: lang
                      },
                    },
                    {
                      slug: {
                        _eq: slug
                      }
                    }
                  ]
                }
              }
            },
          }
        }
      })
    );

    /**
     * Requête pour la relation inverse - événements liés
     * 
     * @type {Record<String, any>[]}
     * @throws Une exception sera lancée s'il y a un problème dans la requête
     */
    const filteredEventsData = await directusClient.request(
      readItems('events', {
        fields: [
          '*',
          'translations.*',
        ],

        // filtrer les événements, pour ne retenir que ceux liés
        // au concept actuel
        filter: {
          concepts_relations: {
            concepts_id: {
              _eq: id
            }
          }
        },

        // désactiver la limite de pagination (au cas où)
        limit: -1,

        // on ne retient dans la charge utile que la traduction dans
        // la `lang` actuelle, ainsi qu'avec le `slug` correspondant
        deep: {
          concepts_relations: {
            concepts_id: {
              translations: {
                _filter: {
                  _and: [
                    {
                      languages_code: {
                        _eq: lang
                      },
                    },
                    {
                      slug: {
                        _eq: slug
                      }
                    }
                  ]
                }
              }
            },
          }
        }
      })
    );

    /**
     * @type {{ relatedProjects: Array<any>; relatedEvents: Array<any>; }}
     */
    return {
      /**
       * Projets liés
       * @type {Array<{ slug: string; }>}
       */
      relatedProjects: filteredProjectsData.map(project => {
        const {
          slug,
          date_start,
          date_end,
          project_status,
          translations
        } = project;
        
        return {
          slug,
          date_start,
          date_end,
          project_status,
          // aplatissement de la traduction avec l'opérateur spread `...`
          ...translations[0]
        };
      }),

      /**
       * Événements liés
       * @type {Array<{ slug: string; }>}
       */
      relatedEvents: filteredEventsData.map(event => {
        const {
          slug,
          date_start,
          date_end,
          time_start,
          time_end,
          event_type,
          translations,
        } = event;

        return {
          slug,
          date_start,
          date_end,
          time_start,
          time_end,
          event_type,
          // aplatissement de la traduction avec l'opérateur spread `...`
          ...translations[0]
        };
      }),

      // aplatissement de la traduction avec l'opérateur spread `...`
      ...translations[0]
    };
  } catch (/** @type {import('@sveltejs/kit').HttpError | any} */e) {
    // Cela pourrait être une erreur `HttpError`, lancée plus haut
    // avec SvelteKit
    if (e?.status === 404) {
      error(404, {
        message: lang === 'en' ? 'The page could not be found.' : 'La page demandée est introuvable.'
      });
    }

    // autre erreur, que nous considérons comme une erreur interne
    console.error(e);
    error(500, {
      message: 'Cette page n’est pas disponible en ce moment (erreur interne). Veuillez revenir plus tard.'
    });
  }
}

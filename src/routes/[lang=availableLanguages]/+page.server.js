import { directusClient } from '$lib/directus';
import { readItems, readSingleton } from '@directus/sdk';
import { error } from '@sveltejs/kit';

/**
 * Données pour la page d'accueil
 * 
 * @type {import('./$types').PageServerLoad}
 */
export async function load({ params }) {
  const { lang } = params;

  try {
    /**
     * Page d'accueil (élément unique, ou singleton)
     * 
     * @type {Record<String, any>}
     * @throws Une exception sera lancée s'il y a un problème dans la requête
     */
    const homeData = await directusClient.request(
      readSingleton('home_index', {
        fields: [
          '*',
          'translations.*',
          'featured_projects_relations.projects_id.translations.*',
          'featured_research_fields_relations.research_fields_id.translations.*',
        ],

        deep: {
          // uniquement la page d'accueil dans la `lang` actuelle
          translations: {
            _filter: {
              languages_code: {
                _eq: lang
              }
            }
          },

          // on récupère uniquement la traduction de chaque projet en vedette
          // dans la `lang` actuelle
          featured_projects_relations: {
            projects_id: {
              translations: {
                _filter: {
                  languages_code: {
                    _eq: lang
                  }
                }
              }
            }
          },
          // on récupère uniquement la traduction de chaque champ de recherche
          // en vedette dans la `lang` actuelle
          featured_research_fields_relations: {   
            research_fields_id: {
              translations: {
                _filter: {
                  languages_code: {
                    _eq: lang
                  }
                }
              }
            }
          }
        },
      })
    );
    
    /**
     * Événements les plus récents à afficher sur la page d'acceuil
     * 
     * @type {Record<String, any>[]}
     * @throws Une exception sera lancée s'il y a un problème dans la requête
     */
    const latestEventsData = await directusClient.request(
      readItems('events', {
        fields: [
          '*',
          'translations.*'
        ],

        // filtrer les événements pour ne garder ceux:
        // 1. dont la date est définie (i.e. non nulle)
        // 2. qui comportent une traduction dans la `lang` actuelle
        filter: {
          // 1.
          'date_start': {
            _nnull: true
          },
          // 2.
          translations: {
            languages_code: {
              _eq: lang
            }
          }
        },

        // trier les événements par plus récents
        sort: ['-date_start'],

        // limiter le nombre d'événements à afficher
        limit: 5,

        // on récupère uniquement la traduction dans la `lang` actuelle,
        // ce qui correspond au champ `languages_code` dans Directus
        deep: {
          translations: {
            _filter: {
              languages_code: {
                _eq: lang
              }
            }
          }
        }
      })
    );

    /**
     * @type {{featuredProjects: any[]; featuredResearchFields: any[]; latestEvents: any[];}}
     */
    return {
      // Données 'home'
      // aplatissement de la traduction avec l'opérateur spread `...`
      ...homeData.translations[0],

      /**
       * Projets mis en vedette
       * @type {Array<any>}
       */
      featuredProjects: homeData.featured_projects_relations?.map(featuredProject => {
        const {
          slug,
          date_start,
          date_end,
          project_status,
          translations
        } = featuredProject.projects_id;
        
        return {
          slug,
          date_start,
          date_end,
          project_status,
          // aplatissement de la traduction avec l'opérateur spread `...`
          ...translations[0]
        }
      }) || [],

      /**
       * Champs de recherche mis en vedette
       * @type {Array<any>}
       */
      featuredResearchFields: homeData.featured_research_fields_relations?.map(featuredResearchField => {
        const {
          slug,
          translations
        } = featuredResearchField.research_fields_id;

        return {
          slug,
          // aplatissement de la traduction avec l'opérateur spread `...`
          ...translations[0]
        };
      }) || [],
      
      /**
       * Événements les plus récents
       * @type {Array<any>}
       */
      latestEvents: latestEventsData.map(event => {
        const {
          slug,
          date_start,
          date_end,
          time_start,
          time_end,
          event_type,
          translations,
        } = event;

        return {
          slug,
          date_start,
          date_end,
          time_start,
          time_end,
          event_type,
          // aplatissement de la traduction avec l'opérateur spread `...`
          ...translations[0]
        };
      }),
    };
  } catch (e) {
    // la page d'équipe est un singleton censé exister; ce n'est pas une
    // erreur 404 introuvable, mais forcément une erreur du côté de
    // l'application (erreur interne, code HTTP 500)
    console.error(e);
    error(500, {
      message: 'La page d’accueil n’est pas disponible pour le moment. Veuillez revenir plus tard.'
    });
  }
}
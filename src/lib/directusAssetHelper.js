/**
 * Fonction utile pour obtenir l'URL d'un fichier dans l'instance Directus,
 * à partir de l'identifiant unique (UUID) de l'image.
 */
import { PUBLIC_DIRECTUS_URL } from '$env/static/public';

/**
 * 
 * @param {string} asset_id UUID du fichier dans l'instance Directus
 * @returns {string} URL adressable du fichier (image, etc)
 */
export default function getAssetUrl(asset_id) {
  return `${PUBLIC_DIRECTUS_URL}/assets/${asset_id}`;
}

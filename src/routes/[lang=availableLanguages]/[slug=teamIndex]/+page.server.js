import { directusClient } from '$lib/directus';
import { readSingleton } from '@directus/sdk';
import { error } from '@sveltejs/kit';

/**
 * Données pour la page d'équipe
 * 
 * @type {import('./$types').PageServerLoad}
 */
export async function load({ params }) {
  const { lang } = params;

  try {
    /**
     * Page d'équipe (élément unique, ou singleton)
     * 
     * @type {Record<String, { persons_members_relations: Array<any>; persons_alumni_relations: Array<any>; translations: Array<{title: string}> }>}
     * @throws Une exception sera lancée s'il y a un problème dans la requête
     */
    const teamIndexData = await directusClient.request(
      readSingleton('team_index', {
        fields: [
          '*',
          'translations.title',
          'persons_members_relations.persons_id.*',
          'persons_alumni_relations.persons_id.*',
          'persons_members_relations.persons_id.translations.*',
          'persons_alumni_relations.persons_id.translations.*',
        ],

        // on récupère uniquement la traduction dans la `lang` actuelle
        deep: {
          persons_members_relations: {
            persons_id: {
              translations: {
                _filter: {
                  languages_code: {
                    _eq: lang
                  }
                }
              }
            }
          },
          persons_alumni_relations: {
            persons_id: {
              translations: {
                _filter: {
                  languages_code: {
                    _eq: lang
                  }
                }
              }
            }
          },
        },
      })
    );

    /**
     * @type {{title: string; members: Array<any>; alumni: Array<any>}}
     */
    return {
      // le titre de la page peut être saisi pour chaque langue
      title: teamIndexData?.translations[0]?.title || '',

      // mappage des membres
      members: teamIndexData.persons_members_relations?.map(member => {
        // les données sont imbriquées dans `persons_id`
        // à cause du champ relationnel
        const {
          first_name,
          last_name,
          slug,
          image_id,
          is_alumnus,
          translations,
          legacy_image,
        } = member.persons_id;

        return {
          first_name,
          last_name,
          slug,
          image_id,
          is_alumnus,
          legacy_image,
          // ... incluant les données traduites
          ...translations[0]
        };
      }) || [],

      // mappage des alumni, exactement comme pour les membres
      alumni: teamIndexData.persons_alumni_relations?.map(member => {
        // les données sont imbriquées dans `persons_id`
        // à cause du champ relationnel
        const {
          first_name,
          last_name,
          slug,
          image_id,
          is_alumnus,
          translations
        } = member.persons_id;

        return {
          first_name,
          last_name,
          slug,
          image_id,
          is_alumnus,
          // ... incluant les données traduites
          ...translations[0]
        };
      }) || [],
    };
  } catch (e) {
    // la page d'équipe est un singleton censé exister; ce n'est pas une
    // erreur 404 introuvable, mais forcément une erreur du côté de
    // l'application (erreur interne, code HTTP 500)
    console.error(e);
    error(500, {
      message: 'La page n’est pas disponible en ce moment (erreur interne). Veuillez revenir plus tard.'
    });
  }
}
/**
 * Les clés sont disposées en ordre alphabétiques.
 * Certaines traductions sont imbriquées dans un espace de nom;
 * elles sont situées plutôt à la fin.
 */
export default {
  'activity': 'Activité',
  'activities': 'Activités',
  'address': 'Adresse',
  'alumnus': 'Alumnus',
  'alumni': 'Alumni',
  'axis': 'Axe de recherche',
  'axis_plural': 'Axes de recherche',
  'bibliographc_references': 'Références bibliographiques',
  'concepts': 'Concepts',
  'context': 'Contexte',
  'data': 'Données',
  'date': 'Date',
  'date_end': 'Date de fin',
  'date_start': 'Date de début',
  'description': 'Description',
  'details': 'Détails',
  'done': 'Terminé',
  'email': 'Courriel',
  'error': 'Erreur',
  'event': 'Événement',
  'events': 'Événements',
  'home': 'Accueil',
  'last_modified_on': 'Dernière modification le',
  'licence': 'Licence',
  'link': 'Lien',
  'links': 'Liens',
  'key_concept': 'Concept clé',
  'key_concepts': 'Concepts clés',
  'next_events': 'Prochains événements',
  'ongoing': 'En cours',
  'page_data': 'Données de la page',
  'person': 'Personne',
  'persons': 'Personnes',
  'phone': 'Téléphone',
  'project': 'Projet',
  'projects': 'Projets',
  'reference_this_page': 'Pour citer cette page',
  'related_event': 'Événement liés',
  'related_events': 'Événements liés',
  'related_project': 'Projet liés',
  'related_projects': 'Projets liés',
  'research_field': 'Champ de recherche',
  'research_fields': 'Champs de recherche',
  'research_object': 'Objet de recherche',
  'research_objects': 'Objets de recherche',
  'see_all_events': 'Voir tous les événements',
  'see_more': 'Voir plus',
  'see_more_ellipsis': 'Voir plus…',
  'show_hide': 'Afficher/masquer',
  'team': 'Équipe',

  /* Espaces de nom */
  
  // page d'accueil
  'homepage': {
    'our_research_fields': 'Nos champs de recherche',
  },

  // page d'équipe
  'teamIndex': {
    'active_members': 'Membres actifs',
    'occupation_status': 'Occupation/statut',
    'role': 'Rôle',
  },

  // page événement
  'event_page': {
    'event_languages': 'Langues de l’événement',
    'event_type': 'Type d’événement',
    'event_type_list': {
      'book_launch': 'Lancement',
      'colloquium': 'Colloque',
      'conference': 'Conférence',
      'interview': 'Entrevue',
      'talk': 'Présentation',
      'workshop': 'Atelier',
    },
    'time_end': 'Heure de fin',
    'time_start': 'Heure de début',
  },

  // langues
  'languages_list': {
    'de': 'Allemand',
    'en': 'Anglais',
    'fr': 'Français',
    'es': 'Espagnol',
    'el': 'Grec',
    'it': 'Italien',
    'la': 'Latin',
  },
}
